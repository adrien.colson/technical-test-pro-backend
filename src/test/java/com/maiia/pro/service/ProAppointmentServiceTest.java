package com.maiia.pro.service;

import com.maiia.pro.EntityFactory;
import com.maiia.pro.entity.Appointment;
import com.maiia.pro.entity.Practitioner;
import com.maiia.pro.exception.NotAvailableException;
import com.maiia.pro.exception.NotImplementedException;
import com.maiia.pro.repository.AppointmentRepository;
import com.maiia.pro.repository.PractitionerRepository;
import com.maiia.pro.repository.TimeSlotRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProAppointmentServiceTest {
    private final  EntityFactory entityFactory = new EntityFactory();
    private  final static Integer patient_id=657679;

    @Autowired
    private ProAppointmentService proAppointmentService;

    @Autowired
    private ProAvailabilityService proAvailabilityService;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private PractitionerRepository practitionerRepository;

    @Autowired
    private TimeSlotRepository timeSlotRepository;

    @Test
    void save() {
        Practitioner practitioner = practitionerRepository.save(entityFactory.createPractitioner());
        LocalDateTime startDate = LocalDateTime.of(2020, Month.FEBRUARY, 5, 11, 0, 0);
        timeSlotRepository.save(entityFactory.createTimeSlot(practitioner.getId(), startDate, startDate.plusHours(1)));
        proAvailabilityService.generateAvailabilities(practitioner.getId());

        List<Appointment> practitionerAppointmentsBeforeSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(0, practitionerAppointmentsBeforeSave.size());

        Appointment appointment = entityFactory.createAppointment(practitioner.getId(),
                patient_id,
                startDate.plusMinutes(30),
                startDate.plusMinutes(45));
        Appointment savedAppointment = proAppointmentService.save(appointment);

        assertNotNull(savedAppointment);
        assertNotNull(savedAppointment.getId());

        List<Appointment> practitionerAppointmentsAfterSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(1, practitionerAppointmentsAfterSave.size());
        assertIterableEquals(List.of(savedAppointment), practitionerAppointmentsAfterSave);
    }

    @Test
    void saveWithExistingAppointment() {
        Practitioner practitioner = practitionerRepository.save(entityFactory.createPractitioner());
        LocalDateTime startDate = LocalDateTime.of(2020, Month.FEBRUARY, 5, 11, 0, 0);
        timeSlotRepository.save(entityFactory.createTimeSlot(practitioner.getId(), startDate, startDate.plusHours(1)));
        Appointment existingAppointment = appointmentRepository.save(
                entityFactory.createAppointment(practitioner.getId(),
                        patient_id,
                        startDate,
                        startDate.plusMinutes(15)));
        proAvailabilityService.generateAvailabilities(practitioner.getId());

        List<Appointment> practitionerAppointmentsBeforeSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(1, practitionerAppointmentsBeforeSave.size());

        Appointment appointment = entityFactory.createAppointment(practitioner.getId(),
                patient_id,
                startDate.plusMinutes(30),
                startDate.plusMinutes(45));
        Appointment savedAppointment = proAppointmentService.save(appointment);

        assertNotNull(savedAppointment);
        assertNotNull(savedAppointment.getId());

        List<Appointment> practitionerAppointmentsAfterSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(2, practitionerAppointmentsAfterSave.size());
        assertIterableEquals(List.of(existingAppointment, savedAppointment), practitionerAppointmentsAfterSave);
    }

    @Test
    void saveWithUnavailableTimeRange() {
        Practitioner practitioner = practitionerRepository.save(entityFactory.createPractitioner());
        LocalDateTime startDate = LocalDateTime.of(2020, Month.FEBRUARY, 5, 11, 0, 0);
        timeSlotRepository.save(entityFactory.createTimeSlot(practitioner.getId(), startDate, startDate.plusHours(1)));
        proAvailabilityService.generateAvailabilities(practitioner.getId());

        List<Appointment> practitionerAppointmentsBeforeSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(0, practitionerAppointmentsBeforeSave.size());

        Appointment appointment = entityFactory.createAppointment(practitioner.getId(),
                patient_id,
                startDate.plusDays(1).plusMinutes(30),
                startDate.plusDays(1).plusMinutes(45));
        assertThrows(NotAvailableException.class, () -> proAppointmentService.save(appointment));

        List<Appointment> practitionerAppointmentsAfterSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(0, practitionerAppointmentsAfterSave.size());
    }

    @Test
    void saveWithOccupiedTimeRange() {
        Practitioner practitioner = practitionerRepository.save(entityFactory.createPractitioner());
        LocalDateTime startDate = LocalDateTime.of(2020, Month.FEBRUARY, 5, 11, 0, 0);
        timeSlotRepository.save(entityFactory.createTimeSlot(practitioner.getId(), startDate, startDate.plusHours(1)));
        Appointment existingAppointment = appointmentRepository.save(
                entityFactory.createAppointment(practitioner.getId(),
                        patient_id,
                        startDate,
                        startDate.plusMinutes(15)));
        proAvailabilityService.generateAvailabilities(practitioner.getId());

        List<Appointment> practitionerAppointmentsBeforeSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(1, practitionerAppointmentsBeforeSave.size());

        Appointment appointment = entityFactory.createAppointment(practitioner.getId(),
                patient_id,
                startDate,
                startDate.plusMinutes(15));
        assertThrows(NotAvailableException.class, () -> proAppointmentService.save(appointment));

        List<Appointment> practitionerAppointmentsAfterSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(1, practitionerAppointmentsAfterSave.size());
    }

    @Test
    void saveWithPartiallyOccupiedTimeRange() {
        Practitioner practitioner = practitionerRepository.save(entityFactory.createPractitioner());
        LocalDateTime startDate = LocalDateTime.of(2020, Month.FEBRUARY, 5, 11, 0, 0);
        timeSlotRepository.save(entityFactory.createTimeSlot(practitioner.getId(), startDate, startDate.plusHours(1)));
        Appointment existingAppointment = appointmentRepository.save(
                entityFactory.createAppointment(practitioner.getId(),
                        patient_id,
                        startDate,
                        startDate.plusMinutes(15)));
        proAvailabilityService.generateAvailabilities(practitioner.getId());

        List<Appointment> practitionerAppointmentsBeforeSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(1, practitionerAppointmentsBeforeSave.size());

        Appointment appointment = entityFactory.createAppointment(practitioner.getId(),
                patient_id,
                startDate.plusMinutes(10),
                startDate.plusMinutes(25));
        assertThrows(NotAvailableException.class, () -> proAppointmentService.save(appointment));

        List<Appointment> practitionerAppointmentsAfterSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(1, practitionerAppointmentsAfterSave.size());
    }

    @Test
    void savePreventsAppointmentUpdate() {
        Practitioner practitioner = practitionerRepository.save(entityFactory.createPractitioner());
        LocalDateTime startDate = LocalDateTime.of(2020, Month.FEBRUARY, 5, 11, 0, 0);
        timeSlotRepository.save(entityFactory.createTimeSlot(practitioner.getId(), startDate, startDate.plusHours(1)));
        Appointment existingAppointment = appointmentRepository.save(
                entityFactory.createAppointment(practitioner.getId(),
                        patient_id,
                        startDate,
                        startDate.plusMinutes(15)));
        proAvailabilityService.generateAvailabilities(practitioner.getId());

        List<Appointment> practitionerAppointmentsBeforeSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(1, practitionerAppointmentsBeforeSave.size());

        Appointment appointment = entityFactory.createAppointment(practitioner.getId(),
                patient_id,
                startDate.plusMinutes(10),
                startDate.plusMinutes(25));
        appointment.setId(existingAppointment.getId());
        assertThrows(NotImplementedException.class, () -> proAppointmentService.save(appointment));

        List<Appointment> practitionerAppointmentsAfterSave = proAppointmentService.findByPractitionerId(practitioner.getId());
        assertEquals(1, practitionerAppointmentsAfterSave.size());
    }

}
