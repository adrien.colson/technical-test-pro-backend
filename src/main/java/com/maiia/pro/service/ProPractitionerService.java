package com.maiia.pro.service;

import com.maiia.pro.dto.PractitionerDTO;
import com.maiia.pro.entity.Practitioner;
import com.maiia.pro.repository.PractitionerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProPractitionerService {
    @Autowired
    private PractitionerRepository practitionerRepository;

    public Practitioner find(Integer practitionerId) {
        return practitionerRepository.findById(practitionerId).orElseThrow();
    }

    public List<Practitioner> findAll() {
        return practitionerRepository.findAll();
    }

    public List<PractitionerDTO> findAllDTO() {
        return practitionerRepository.findDTOBy();
    }

}
