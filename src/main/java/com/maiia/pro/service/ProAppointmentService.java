package com.maiia.pro.service;

import com.maiia.pro.entity.Appointment;
import com.maiia.pro.entity.Availability;
import com.maiia.pro.exception.NotAvailableException;
import com.maiia.pro.exception.NotImplementedException;
import com.maiia.pro.repository.AppointmentRepository;
import com.maiia.pro.repository.AvailabilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class ProAppointmentService {
    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private AvailabilityRepository availabilityRepository;

    public Appointment find(Integer appointmentId) {
        return appointmentRepository.findById(appointmentId).orElseThrow();
    }

    @Transactional
    public Appointment save(Appointment appointment) {
        Assert.notNull(appointment, "appointment cannot be null.");

        if (appointment.getId() != null) {
            throw new NotImplementedException("Updating an appointment is not implemented.");
        }

        Availability availability = availabilityRepository.findByPractitionerIdAndStartDateAndEndDate(
                        appointment.getPractitionerId(),
                        appointment.getStartDate(),
                        appointment.getEndDate())
                .orElseThrow(() -> new NotAvailableException("This practitioner has no availability for this time range."));

        availabilityRepository.delete(availability);

        return appointmentRepository.save(appointment);
    }

    public List<Appointment> findAll() {
        return appointmentRepository.findAll();
    }

    public List<Appointment> findByPractitionerId(Integer practitionerId) {
        return appointmentRepository.findByPractitionerId(practitionerId);
    }
}
