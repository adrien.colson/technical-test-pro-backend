package com.maiia.pro.service;

import com.maiia.pro.entity.Appointment;
import com.maiia.pro.entity.Availability;
import com.maiia.pro.entity.TimeSlot;
import com.maiia.pro.repository.AppointmentRepository;
import com.maiia.pro.repository.AvailabilityRepository;
import com.maiia.pro.repository.TimeSlotRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class ProAvailabilityService {

    @Autowired
    private AvailabilityRepository availabilityRepository;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private TimeSlotRepository timeSlotRepository;

    @Value("${availability.duration}")
    private Duration availabilityDuration;

    public List<Availability> findByPractitionerId(Integer practitionerId) {
        return availabilityRepository.findByPractitionerId(practitionerId);
    }

    @Transactional
    public List<Availability> generateAvailabilities(Integer practitionerId) {
        availabilityRepository.deleteByPractitionerId(practitionerId);

        List<TimeSlot> timeSlots = timeSlotRepository.findByPractitionerId(practitionerId);
        List<Appointment> appointments = appointmentRepository.findByPractitionerId(practitionerId);
        return timeSlots.stream()
                .flatMap(timeSlot -> streamTimeSlotAvailabilities(timeSlot, appointments))
                .map(availabilityRepository::save)
                .collect(Collectors.toList());
    }

    private Stream<Availability> streamTimeSlotAvailabilities(TimeSlot timeSlot, List<Appointment> appointments) {
        Availability firstAvailability = makeFirstCheckedAvailability(timeSlot.getPractitionerId(), timeSlot.getStartDate(), appointments);

        UnaryOperator<Availability> makeNextAvailability = availability -> makeFirstCheckedAvailability(availability.getPractitionerId(), availability.getEndDate(), appointments);

        Predicate<Availability> isStillInTimeSlot = availability -> !availability.getEndDate().isAfter(timeSlot.getEndDate());

        return Stream.iterate(firstAvailability, makeNextAvailability).takeWhile(isStillInTimeSlot);
    }

    private Availability makeFirstCheckedAvailability(Integer practitionerId, LocalDateTime fromDate, List<Appointment> appointments) {
        Availability availability = makeAvailability(practitionerId, fromDate);
        Optional<LocalDateTime> collisionEndDate;
        while ((collisionEndDate = getCollisionEndDate(availability, appointments)).isPresent()) {
            availability = makeAvailability(practitionerId, collisionEndDate.get());
        }
        return availability;
    }

    private Availability makeAvailability(Integer practitionerId, LocalDateTime startDate) {
        return Availability.builder()
                .practitionerId(practitionerId)
                .startDate(startDate)
                .endDate(startDate.plus(availabilityDuration))
                .build();
    }

    private Optional<LocalDateTime> getCollisionEndDate(Availability availability, List<Appointment> appointments) {
        return appointments.stream()
                .filter(appointment -> hasCollision(availability, appointment))
                .map(Appointment::getEndDate)
                .max(LocalDateTime::compareTo);
    }

    public static boolean hasCollision(Availability availability, Appointment appointment) {
        return availability.getStartDate().isBefore(appointment.getEndDate())
                && availability.getEndDate().isAfter(appointment.getStartDate());
    }

}
