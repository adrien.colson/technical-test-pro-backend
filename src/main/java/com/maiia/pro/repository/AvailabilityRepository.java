package com.maiia.pro.repository;

import com.maiia.pro.entity.Availability;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface AvailabilityRepository extends CrudRepository<Availability, Integer> {
    List<Availability> findByPractitionerId(Integer id);

    Optional<Availability> findByPractitionerIdAndStartDateAndEndDate(Integer id, LocalDateTime startDate, LocalDateTime endDate);

    void deleteByPractitionerId(Integer id);

}
