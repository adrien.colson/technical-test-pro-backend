package com.maiia.pro.dto;

import lombok.Value;

@Value
public class PatientDTO {
    Integer id;
    String firstName;
    String lastName;
}
