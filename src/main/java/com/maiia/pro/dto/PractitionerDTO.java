package com.maiia.pro.dto;

import lombok.Value;

@Value
public class PractitionerDTO {
    Integer id;
    String firstName;
    String lastName;
}
